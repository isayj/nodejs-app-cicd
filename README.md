# nodejs-app-cicd

## Description
A small hands-on project with Node.js app to get to know how CI/CD works and beyond.

## Roadmap
1. Set up basic Node.js app
2. Create CI/CD pipeline file with special naming convention `.gitlab-ci.yml` for GitLab Runner(shared SaaS runners)
3. Rearrange the stages order so build can always be ahead of deploy
4. Create job artifacts so that the build files can be passed from one stage to another
5. Define image services used for all jobs
6. Switch to self-imaged Runner registered with Windows
7. Switch to self-imaged Runner registered with the Amazon EC2 instance

...

## Authors and acknowledgment
Big applause to [Nasiullha Chaudhari](https://www.linkedin.com/in/nasiullha-chaudhari-5a80601a8/) who teaches this on [Cloud Champ](https://www.youtube.com/watch?v=JWXVijJfnHc&t) Youtube channel.

## Project status
Step 6 Done
